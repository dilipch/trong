class Users::RegistrationsController < Devise::RegistrationsController
# before_action :configure_sign_up_params, only: [:create]
# before_action :configure_account_update_params, only: [:update]

  # GET /resource/sign_up
  def new
    build_resource({referral_code: params.delete(:referral_code)})
    yield resource if block_given?
    respond_with resource
  end
end
