class AddCurrentAmmountToUser < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :current_amount_in_cents, :decimal, precision: 8, scale: 2
  end
end
