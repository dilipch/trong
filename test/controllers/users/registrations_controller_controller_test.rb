require 'test_helper'

class Users::RegistrationsControllerControllerTest < ActionDispatch::IntegrationTest

  test "Sign-up without referral code" do
    post user_registration_path({user: {email: 'test@xyz.com', password: '12345678',password_confirmation: '12345678' }})
    assert_response :redirect
    user = User.find_by(email: 'test@xyz.com')
    assert_instance_of User, user
    assert_instance_of ReferralCode, user.referral
    assert_nil user.referral_by_code
  end

  test "Sign-up with invalid referral code" do
    post user_registration_path({user: {email: 'test@xyz.com', password: '12345678',password_confirmation: '12345678', referral_code: 'in-valid'}})
    assert_response :success
    user = User.find_by(email: 'test@xyz.com')
    assert_nil user
  end

  test "Sign-up with valid referral code" do
    post user_registration_path({user: {email: 'test@xyz.com', password: '12345678',password_confirmation: '12345678', referral_code: users(:one).referral.code}})
    assert_response :redirect
    user = User.find_by(email: 'test@xyz.com')
    assert_instance_of User, user
    assert_instance_of ReferralCode, user.referral
    assert_instance_of ReferralCode, user.referral_by_code
    assert_equal users(:one).referral, user.referral_by_code
  end

end
