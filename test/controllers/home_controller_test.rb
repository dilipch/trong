require 'test_helper'

class HomeControllerTest < ActionDispatch::IntegrationTest
  test "should redirect to login page" do
    get home_index_url
    assert_response :redirect
    follow_redirect!
    assert_response :success
  end

  test "should access homepage when user logged-in" do
    user = User.create(email: 'test1@xyz.com', password: '12345678')
    post user_session_path({user: {email: user.email, password: '12345678'}})

    assert_response :redirect
    follow_redirect!
    assert_response :success

    get home_index_url
    assert_response :success
  end

end
