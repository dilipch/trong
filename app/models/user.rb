class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  attr_accessor :referral_code
  validate :check_referral_code, on: :create, if: 'referral_code.present?'
  after_create :post_processing

  has_one :referral, class_name: 'ReferralCode', foreign_key: 'user_id'
  belongs_to :referral_by_code, class_name: 'ReferralCode', foreign_key: 'referral_code_id', optional: true
  has_many :account_summaries

  def credit_amount amount_to_add, description=nil
    ActiveRecord::Base.transaction do
      update(current_amount_in_cents: self.current_amount_in_cents.to_i + amount_to_add*100)
      account_summaries.create(amount_in_cents: amount_to_add*100, credit: true, desciption: description)
    end
  end

  def account_balance_in_cents
    current_amount_in_cents.to_i
  end

  private

  def check_referral_code
    errors.add(:referral_code, "not valid") unless get_referral_by_code
  end

  def post_processing
    create_referral
    process_referral_code
  end

  def process_referral_code
    # Setting refer_by
    if get_referral_by_code
      self.referral_by_code = get_referral_by_code
      apply_referral_offer if save
    end
  end

  def apply_referral_offer
    get_referral_by_code.user.credit_amount(20, "New user sign-up with user's invitation code #{get_referral_by_code}")
    credit_amount(20, "Sign-up promo with code #{get_referral_by_code.code}")
  end

  def get_referral_by_code
    @referral_by_code ||= ReferralCode.find_by(code: self.referral_code)
  end

end
