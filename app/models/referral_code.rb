class ReferralCode < ApplicationRecord
  belongs_to :user
  has_many :referral_users, class_name: 'User', foreign_key: 'referral_code_id'

  validates :code, presence: true, uniqueness: true
  before_validation :generate_code, on: :create

  private
  def generate_code
    self.code = SecureRandom.urlsafe_base64(4)
    generate_code if ReferralCode.exists?(code: self.code)
  end
end
