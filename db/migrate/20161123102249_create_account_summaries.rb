class CreateAccountSummaries < ActiveRecord::Migration[5.0]
  def change
    create_table :account_summaries do |t|
      t.references :user, foreign_key: true
      t.text :desciption
      t.decimal :amount_in_cents, precision: 8, scale: 2

      t.boolean :credit

      t.timestamps
    end
  end
end
