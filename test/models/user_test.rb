require 'test_helper'

class UserTest < ActiveSupport::TestCase

  test "Save user without email and password" do
    user = User.new
    assert_not user.save
  end

  test "Save user without password" do
    user = User.new
    user.email = 'test4@yopmail.com'
    assert_not user.save
  end

  test "Save user without email" do
    user = User.new
    user.password = 'test4@yopmail.com'
    assert_not user.save
  end

  test "Save user email and password" do
    user = User.new
    user.email = 'test3@yopmail.com'
    user.password = '12345678'
    assert user.save
  end

  test "Save user without referral code" do
    user = User.new
    user.email = 'test5@yopmail.com'
    user.password = '12345678'
    assert user.save
    assert_nil user.referral_by_code
  end

  test "Save user with in-valid referral code" do
    user = User.new
    user.email = 'test6@yopmail.com'
    user.password = '12345678'
    user.referral_code = 'invalid'
    assert_not user.save
    assert_nil user.referral_by_code
  end

  test "Save user with valid referral code" do
    referral_old_balance = referral_codes(:one).user.current_amount_in_cents.to_i
    user = User.new
    user.email = 'test6@yopmail.com'
    user.password = '12345678'
    user.referral_code = referral_codes(:one).code
    assert user.save
    assert_instance_of ReferralCode, user.referral_by_code
    assert_equal user.current_amount_in_cents.to_i, 2000
    assert_equal 2000, user.referral_by_code.user.current_amount_in_cents.to_i - referral_old_balance
  end

end
